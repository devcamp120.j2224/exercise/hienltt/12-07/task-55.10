package com.devcamp.task5510.restapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class CDailyCampaign {
    String date;
    @GetMapping("/pizza365/promote")
    public String promote(){
        DateTimeFormatter dtfVietNam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());
        String vThu = dtfVietNam.format(today); //format lại today
        CDailyCampaign daily = new CDailyCampaign(); //tạo đối tượng để gọi method
        String vKetQua =  daily.thongTin(vThu); //khai báo biến chứa kết quả trả về
        return vKetQua;
    }
    
    public String thongTin(String strThu){
        String vThongBao = "";
        if(strThu.indexOf("Thứ Hai") == 0 ){
            vThongBao = strThu + ", mua 1 tặng 1";
        }else  if(strThu.indexOf("Thứ Ba") == 0 ){
            vThongBao = strThu + ", mua 1 tặng 2";
        }else {
            vThongBao = strThu + ", tặng 1 phần Coca";
        }
        return vThongBao;
    }
      
}
