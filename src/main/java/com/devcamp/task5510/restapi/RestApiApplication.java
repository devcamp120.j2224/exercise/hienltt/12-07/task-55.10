package com.devcamp.task5510.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
		CDailyCampaign today = new CDailyCampaign();
		System.out.println(today.promote());
	}

}
